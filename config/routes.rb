Rails.application.routes.draw do

  resources :chapters do
    resources :songs, shallow: true, except: [:show, :index]
    put 'move', on: :member
  end

  root 'chapters#index'
  devise_for :users
  get 'profile', to: 'home#profile'
  put 'profile', to: 'home#update_profile'

  mount API => '/'
end
