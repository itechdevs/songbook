# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email
# Environment variables (ENV['...']) can be set in the file .env file.

# Fill data
if Rails.env.development?
  while Chapter.count < 10
    Chapter.create! name: Faker::Lorem.sentence
  end

  Chapter.all.each do |chapter|
    while chapter.songs.count < 10
      chapter.songs.create! name: Faker::Lorem.sentence, text: Faker::Lorem.sentences(20).join("\r\n")
    end
  end
end