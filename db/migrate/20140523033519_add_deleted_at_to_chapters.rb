class AddDeletedAtToChapters < ActiveRecord::Migration
  def change
    add_column :chapters, :deleted_at, :datetime, default: nil
  end
end
