class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.string :name
      t.text :text
      t.belongs_to :chapter, index: true

      t.timestamps
    end
  end
end
