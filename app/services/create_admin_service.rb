class CreateAdminService
  def call
    user = User.where(name: Rails.application.secrets.admin_name).first_or_create do |user|
        user.email = Rails.application.secrets.admin_email
        user.password = Rails.application.secrets.admin_password
        user.password_confirmation = Rails.application.secrets.admin_password
      end
  end
end
