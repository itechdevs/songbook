class HomeController < ApplicationController

  def profile
    @user = current_user
  end

  def update_profile
    @user = current_user
    if @user.update_attributes user_params
      redirect_to root_path, notice: t('users.profile_updated')
    else
      render 'profile'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

end
