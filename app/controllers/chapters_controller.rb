class ChaptersController < ApplicationController
  before_action :set_chapter, only: [:show, :edit, :update, :move, :destroy]

  def index
    @chapters = Chapter.ordered
    respond_to do |format|
      format.html
      format.js { render 'shared/index' }
    end
  end

  def show
    @songs = @chapter.songs
  end

  def new
    @chapter = Chapter.new
    respond_to do |format|
      format.html { render 'form' }
    end
  end

  def edit
    respond_to do |format|
      format.html { render 'form' }
    end
  end

  def create
    @chapter = Chapter.new(chapter_params)
    respond_to do |format|
      if @chapter.save
        format.html { redirect_to @chapter, notice: t('chapters.created') }
      else
        format.html { render 'form' }
      end
    end
  end

  def update
    respond_to do |format|
      if @chapter.update(chapter_params)
        format.html { redirect_to @chapter, notice: t('chapters.updated') }
      else
        format.html { render 'form' }
      end
    end
  end

  def move
    @chapter.move_to params[:destination]
    @chapters = Chapter.ordered
    render 'shared/index'
  end

  def destroy
    # @chapter.destroy
    @chapter.mark_deleted
    respond_to do |format|
      format.html { redirect_to chapters_url, notice: t('chapters.deleted') }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chapter
      @chapter = Chapter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chapter_params
      params.require(:chapter).permit(:name, :position)
    end
end
