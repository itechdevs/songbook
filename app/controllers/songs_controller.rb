class SongsController < ApplicationController
  before_action :set_song, only: [:edit, :update, :destroy]

  def new
    @chapter = Chapter.find params[:chapter_id]
    @song = @chapter.songs.build
    respond_to do |format|
      format.html { render 'form' }
    end
  end

  def edit
    respond_to do |format|
      format.html { render 'form' }
    end
  end

  def create
    @chapter = Chapter.find params[:chapter_id]
    @song = @chapter.songs.build song_params
    respond_to do |format|
      if @song.save
        format.html { redirect_to @song.chapter, notice: t('songs.created') }
      else
        format.html { render 'form' }
      end
    end
  end

  def update
    respond_to do |format|
      if @song.update(song_params)
        format.html { redirect_to @song.chapter, notice: t('songs.updated') }
      else
        format.html { render 'form' }
      end
    end
  end

  def destroy
    # @song.destroy
    chapter = @song.chapter
    @song.mark_deleted
    respond_to do |format|
      format.html { redirect_to chapter, notice: t('songs.destroyed') }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_song
      @song = Song.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def song_params
      params.require(:song).permit(:name, :text, :chapter_id)
    end
end
