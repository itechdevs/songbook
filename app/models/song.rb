class Song < ActiveRecord::Base
  include DeleteMarkable
  belongs_to :chapter, inverse_of: :songs
  validates_presence_of :name, :chapter
end
