module DeleteMarkable
  extend ActiveSupport::Concern

  included do
    default_scope { not_deleted }
    scope :not_deleted, -> { where deleted_at: nil }
    scope :deleted, -> { unscope(:where).where.not deleted_at: nil }

    def mark_deleted
      self.update_attribute :deleted_at, Time.current
    end
  end
end