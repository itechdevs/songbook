class Chapter < ActiveRecord::Base
  include DeleteMarkable
  scope :ordered, -> { order(:position) }
  has_many :songs, inverse_of: :chapter, dependent: :destroy
  validates_presence_of :name
  acts_as_list

  def move_to(destination)
    case destination
      when 'top' then self.move_to_top
      when 'up' then self.move_higher
      when 'down' then self.move_lower
      when 'bottom' then self.move_to_bottom
    end
  end
end
