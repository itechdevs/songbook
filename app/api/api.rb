class API < Grape::API
  prefix 'api'
  version 'v1', using: :accept_version_header
  default_format :json
  format :json

  helpers do
    def logger
      API.logger
    end
  end

  desc 'Get updates'
  get 'updates' do
    time = params[:time].try :to_time
    is_full = params[:full].present?
    current_time = Time.current.getutc
    chapters = Chapter.not_deleted
    songs = time.present? ? Song.where(updated_at: time..current_time) : Song.all
    deleted_songs = time.present? ? Song.unscoped.where(deleted_at: time..current_time) : Song.unscoped.deleted
    if is_full
      {time: current_time, chapters: chapters.as_json(only: [:id, :name, :position]), songs: songs.as_json(only:  [:id, :name, :text, :chapter_id]), deleted_songs: deleted_songs.map(&:id)}
    else
      {updated_songs: songs.count, deleted_songs: deleted_songs.count}
    end
  end

end