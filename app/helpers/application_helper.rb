module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def glyph(*names)
    names.map! { |name| name.to_s.gsub('_','-') }
    names.map! do |name|
      name =~ /pull-(?:left|right)/ ? name : "glyphicon glyphicon-#{name}"
    end
    content_tag :span, nil, class: names
  end

  def auto_title(object=nil)
    object.present? ? (object.new_record? ? t("#{object.class.to_s.tableize}.new.title") : t("#{object.class.to_s.tableize}.edit.title")) : t('.title', default: controller_name.classify.constantize.model_name.human)
  end

  def auto_header_tag(object=nil, title=nil, button_name=nil)
    model_class = object.present? ? object.class : controller_name.classify.constantize
    content_tag :div, class: 'page-header' do
      if action_name == 'index'
        content_tag(:h1, t('.title'), class: 'title') + ((can?(:create, model_class)) ? link_to_new(model_class, button_name) : '')
      else
        title ||= auto_title object
        content_tag :h1, title, class: 'title'
      end
    end
  end

  def submit_button(form, options = {})
    options.merge! class: 'submit btn btn-primary ' + (options[:class] || ''), type: 'submit'
    model_name = form.object.class.model_name.element
    human_model_name = form.object.class.model_name.human
    action = form.object.new_record? ? 'create' : 'update'
    if options[:name] == false
      name = nil
    else
      name = options[:name] || t("helpers.submit.#{model_name}.#{action}", model: human_model_name, default: t(action, default: 'Save'))
    end
    button_tag name, options
  end

end
