require 'spec_helper'

# Feature: Navigation links
#   As a visitor
#   I want to see navigation links
#   So I can find home, sign in, or sign up
feature 'Navigation links', :devise do

  # Scenario: View navigation links
  #   Given I am a visitor
  #   When I visit the home page
  #   Then I see "home," "sign in," and "sign up"
  scenario 'view navigation links' do
    visit root_path
    expect(page).to have_content I18n.t('application_name')
    expect(page).to have_content I18n.t('devise.sessions.sign_in')
  end

end

feature 'Navigation links for authorized user', :devise do
  background { @user = User.create name: 'user', email: 'user@mail.com', password: 'password' }

  scenario 'authorization' do
    visit new_user_session_path
    log_in_with 'user', 'password'
    expect(page).to have_content I18n.t('devise.sessions.signed_in')
  end

  scenario 'view navigation links' do
    log_in_with 'user', 'password'
    visit root_path
    expect(page).to have_content I18n.t('profile')
    expect(page).to have_content I18n.t('devise.sessions.sign_out')
    expect(page).to have_content I18n.t('chapters.index.title')
  end

  def log_in_with(login, password)
    visit new_user_session_path
    within '#new_user' do
      fill_in 'user_login', with: login
      fill_in 'user_password', with: password
      click_button I18n.t('devise.sessions.new.submit', default: 'Sign in')
    end
  end

end
