require 'spec_helper'

describe SongsController, type: :routing do
  describe 'routing' do

    it 'routes to #new' do
      expect(get: '/chapter/1/songs/new').to route_to('songs#new', chapter_id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/songs/1/edit').to route_to('songs#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/chapters/1/songs').to route_to('songs#create', chapter_id: '1')
    end

    it 'routes to #update' do
      expect(put: '/songs/1').to route_to('songs#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/songs/1').to route_to('songs#destroy', id: '1')
    end

  end
end
