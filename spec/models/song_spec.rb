require 'spec_helper'

describe Song, type: :model do

  it 'is invalid without name' do
    expect(build(:song, :without_name)).to_not be_valid
  end

  it 'is invalid without chapter' do
    expect(build(:song, :without_chapter)).to_not be_valid
  end

  it 'is valid with valid attributes' do
    expect(build(:song)).to be_valid
  end

  context 'delete markable' do
    subject(:song) { create :song }

    it 'is marking deleted' do
      song.mark_deleted
      expect(song.deleted_at.to_s).to eq(Time.current.to_s)
    end

    it 'has deleted scope' do
      deleted_song = create :song, :deleted
      expect(Song.deleted).to eq([deleted_song])
    end

    it 'has not_deleted scope and it is default' do
      create :song, :deleted
      expect(Song.not_deleted).to eq([song])
      expect(Song.all).to eq([song])
    end

  end

end
