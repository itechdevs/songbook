require 'spec_helper'

describe Chapter, type: :model do

  it 'is invalid without name' do
    expect(build(:chapter, :without_name)).to_not be_valid
  end

  it 'is valid with valid attributes' do
    expect(build(:chapter)).to be_valid
  end

  # it 'orders by position' do
  #   chapter1 = create :chapter, position: 2
  #   chapter2 = create :chapter, position: 3
  #   chapter3 = create :chapter, position: 1
  #   expect(Chapter.all).to eq([chapter3, chapter1, chapter2])
  # end

  context 'delete markable' do
    subject(:chapter) { create :chapter }

    it 'is marking deleted' do
      chapter.mark_deleted
      expect(chapter.deleted_at.to_s).to eq(Time.current.to_s)
    end

    it 'has deleted scope' do
      deleted_chapter = create :chapter, :deleted
      expect(Chapter.deleted).to eq([deleted_chapter])
    end

    it 'has not_deleted scope and it is default' do
      create :chapter, :deleted
      expect(Chapter.not_deleted).to eq([chapter])
      expect(Chapter.all).to eq([chapter])
    end

  end

end
