# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :song do
    chapter
    name Faker::Lorem.sentence
    text Faker::Lorem.sentences(20).join("\r\n")

    trait(:deleted) { deleted_at Time.current }
    trait(:without_name) { name nil }
    trait(:without_chapter) { chapter_id nil }
  end
end
