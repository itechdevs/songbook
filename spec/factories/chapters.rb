# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :chapter do
    sequence(:name) {|n|"Chapter #{n}"}
    sequence(:position) {|n|n}

    trait(:deleted) { deleted_at Time.current }
    trait(:without_name) { name nil }
  end
end
